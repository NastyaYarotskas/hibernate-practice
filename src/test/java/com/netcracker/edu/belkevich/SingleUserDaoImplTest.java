package com.netcracker.edu.belkevich;


import com.netcracker.edu.belkevich.hibernate.simple.dao.UserDao;
import com.netcracker.edu.belkevich.hibernate.simple.dao.impl.UserDaoImpl;
import com.netcracker.edu.belkevich.hibernate.simple.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class SingleUserDaoImplTest {

    private UserDao userDao;

    private User testUser;

    @BeforeEach
    public void setUp() {
        userDao = new UserDaoImpl();
        testUser = new User("Bruce");
        System.out.println("----- Test user: " + testUser);
    }

    @Test
    public void testCreateSingleUser() {
        User createdUser = userDao.create(testUser);
        System.out.println("----- Created user: " + createdUser);

        assertNotNull(createdUser.getId());
    }

    @Test
    public void testFindSingleUser() {
        User createdUser = userDao.create(testUser);
        System.out.println("----- Created user: " + createdUser);

        User foundUser = userDao.find(createdUser.getId());
        System.out.println("----- Found user: " + foundUser);

        assertNotNull(foundUser);
        assertEquals(createdUser.getId(), foundUser.getId());
        assertEquals(createdUser.getName(), foundUser.getName());
    }

    @Test
    public void testUpdateSingleUser() {
        User createdUser = userDao.create(testUser);
        System.out.println("----- Created user: " + createdUser);

        createdUser.setName("Batman");
        User updatedUser = userDao.update(createdUser);
        System.out.println("----- Updated user: " + updatedUser);

        assertNotNull(updatedUser);
        assertEquals(createdUser.getId(), updatedUser.getId());
        assertEquals(createdUser.getName(), updatedUser.getName());
    }

    @Test
    public void testDeleteSingleUser() {
        User createdUser = userDao.create(testUser);
        System.out.println("----- Created user: " + createdUser);

        Long id = createdUser.getId();
        userDao.delete(id);
        User deletedUser = userDao.find(id);

        assertNull(deletedUser);
    }

}