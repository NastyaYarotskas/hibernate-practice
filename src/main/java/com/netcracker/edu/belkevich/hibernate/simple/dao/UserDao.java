package com.netcracker.edu.belkevich.hibernate.simple.dao;

import com.netcracker.edu.belkevich.hibernate.simple.entity.User;

import java.util.List;

public interface UserDao {

    User create(User user);

    User find(Long id);

    List<User> findAll();

    User update(User user);

    void delete(Long id);

}
