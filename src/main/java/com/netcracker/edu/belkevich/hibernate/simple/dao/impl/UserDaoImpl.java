package com.netcracker.edu.belkevich.hibernate.simple.dao.impl;

import com.netcracker.edu.belkevich.hibernate.simple.dao.UserDao;
import com.netcracker.edu.belkevich.hibernate.simple.entity.User;
import com.netcracker.edu.belkevich.hibernate.utils.PostgreSQLDatabaseManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class UserDaoImpl implements UserDao {

    private EntityManager entityManager = PostgreSQLDatabaseManager.getInstance().getEntityManager();

    public UserDaoImpl() {
    }

    @Override
    public User create(User user) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.persist(user);
        tx.commit();
        return user;
    }

    @Override
    public User find(Long id) {
        EntityTransaction tx = entityManager.getTransaction();
        User foundUser;
        tx.begin();
        foundUser = entityManager.find(User.class, id); //not found can return null
        //foundUser = entityManager.getReference(User.class, id); //exception
        tx.commit();
        return foundUser;
    }

    @Override
    public List<User> findAll() {
        EntityTransaction tx = entityManager.getTransaction();
        List<User> users;
        tx.begin();
        users = entityManager.createQuery("SELECT user FROM User user", User.class).getResultList();
        tx.commit();
        return users;
    }

    @Override
    public User update(User user) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.merge(user);
        tx.commit();
        return user;
    }

    @Override
    public void delete(Long id) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        User user = entityManager.getReference(User.class, id);
        entityManager.remove(user);
        tx.commit();
    }
}
