package com.netcracker.edu.belkevich.code.entity;

public enum Relation {

    FRIEND,
    RELATIVE,
    COLLEAGUE
}
