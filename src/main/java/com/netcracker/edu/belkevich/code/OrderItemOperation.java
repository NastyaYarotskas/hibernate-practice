package com.netcracker.edu.belkevich.code;

import java.util.function.Function;

@FunctionalInterface
public interface OrderItemOperation extends Function<OrderItem, OrderItem> {
}
